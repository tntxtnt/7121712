// compile with
// emcc -std=c++17 -O3 --bind -o num2words.js main.cpp
#include <algorithm>
#include <cctype>
#include <emscripten/bind.h>
#include <sstream>
#include <vector>

using namespace emscripten;

static const std::string CHUSO[] = {"không", "một", "hai", "ba",   "bốn", "năm",
                                    "sáu",   "bảy", "tám", "chín", "mười"};
static const std::string DONVI[] = {"", "nghìn", "triệu", "tỷ"};
static const std::string LINH = "linh";
static const std::string MUOI = "mươi";
static const std::string MOTS = "mốt";
static const std::string TUW = "tư";
static const std::string LAMW = "lăm";
static const std::string TRAMW = "trăm";
static const std::string ERROR_MSG = "Lỗi: Số có ký tự không hợp lệ";

std::string num2words(std::string n)
{
    if (n.empty())
        return "";
    if (std::any_of(begin(n), end(n),
                    [](char c) { return !std::isdigit((unsigned char)c); }))
        return ERROR_MSG;
    std::reverse(begin(n), end(n));
    for (char& c : n)
        c -= '0';
    while (!n.empty() && !n.back())
        n.pop_back();
    if (n.empty())
        return CHUSO[0];
    std::vector<std::string> s;
    std::string billionth;
    for (size_t i = 0; i < n.size(); i += 9)
    {
        if (i)
            s.push_back(billionth);
        for (size_t j = i, jj = 0; jj < 3 && j < n.size(); ++jj, j += 3)
        {
            size_t j1 = j + 1, j2 = j + 2;
            if (jj) // chữ số hàng ngàn(jj=1)/triệu(jj=2)
                s.push_back(DONVI[jj]);
            if (n[j]) // hàng đơn vị (khác 0 mới đọc)
            {
                if (n[j] == 1 && j1 < n.size() && n[j1] > 1)
                    s.push_back(MOTS);
                else if (n[j] == 4 && j1 < n.size() && n[j1] > 1)
                    s.push_back(TUW);
                else if (n[j] == 5 && j1 < n.size() && n[j1])
                    s.push_back(LAMW);
                else
                    s.push_back(CHUSO[n[j]]);
            }
            if (j1 < n.size()) // hàng chục
            {
                switch (n[j1])
                {
                case 0:
                    if (n[j])
                        s.push_back(LINH);
                    break;
                case 1:
                    s.push_back(CHUSO[10]);
                    break;
                default:
                    s.push_back(MUOI), s.push_back(CHUSO[n[j1]]);
                    break;
                }
            }
            if (j2 < n.size()) // hàng trăm
            {
                if (n[j] || n[j1] || n[j2])
                    s.push_back(TRAMW), s.push_back(CHUSO[n[j2]]);
            }
            if (!s.empty() && s.back() == DONVI[jj]) // <none> ngàn/<none> triệu
                s.pop_back(); // xóa <none> ngàn/<none> triệu
        }
        if (!s.empty() && s.back() == billionth)
            s.pop_back();
        if (i)
            billionth += ' ';
        billionth += DONVI[3];
    }
    std::reverse(begin(s), end(s));
    // using sstream increase size:
    // 113KB .js + 149KB .wasm or 432KB single .js (-s WASM=0)
    // not using sstream: 35KB + 21KB or 77KB
    std::string result;
    for (size_t i = 0; i < s.size() - 1; ++i)
        result += s[i], result += ' ';
    if (!s.empty())
        result += s.back();
    return result;
}

EMSCRIPTEN_BINDINGS(my_module)
{
    function("num2words", &num2words);
}
