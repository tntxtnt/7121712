var Module = {
  onRuntimeInitialized: function() {
    var input = document.getElementById("input");
    input.onkeyup = function() {
      document.getElementById("result").innerHTML =
        Module.num2words(input.value);
    }
  }
};